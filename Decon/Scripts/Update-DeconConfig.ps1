﻿function Update-DeconConfig()
{
    Param(
    [string]$Path
    )

    $Configuration = get-content -Path $Path | convertfrom-json | ConvertFrom-JsonCustomObject

    $FolderLoaction = ($Path.Split("\")[0..($Path.Split("\").length - 4)] -join "\")

    $ListOfSettings = @("UserConfiguration", "LogLocation", "CIFSServerCredLocation", "UnpackingToolLocation")
    foreach ($Setting in $ListOfSettings)
    {
        if($Setting -eq "UnpackingToolLocation")
        {
            $ModuleLocation = $Configuration.Settings.$Setting.split("\")[-4..-1] -join "\"
            $Configuration.settings.$setting = Join-Path $FolderLoaction $ModuleLocation
        }
        else
        {
            $ModuleLocation = $Configuration.Settings.$Setting.split("\")[-3..-1] -join "\"
            $Configuration.settings.$setting = Join-Path $FolderLoaction $ModuleLocation
        }
        <#
        join is taking the literal position givin from the split and putting them together.
        It appears we are calling the items in reverse but the way join interprets the list 
        is somehow based on origanol postion in the string. I have no real idea whats happening.
        If you test this on the commadline it will work as expected but in the loop it reverses the order.
        cheers = )
        #>
    }
    $Configuration | convertto-json | out-file -FilePath $Path -Force
}