﻿function Invoke-DetectionModuleManager()
{
    Param(
        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [string]$DecontaminationStagingArea,
        
        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [string]$DLPStagingArea,

        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [string]$EventID,
        
        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [string]$FilePath,
        
        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [string]$UserName,
        
        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [string]$StagingAreaName,

        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [string]$TrustHomeFolder,

        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [string]$UnTrustHomeFolder,

        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [string]$QuarantineFolder,

        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [string]$UnpackingToolLocation,

        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [string]$ActionThreshHold,

        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [hashtable]$DetectionModulesSettings,

        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [hashtable]$FileDB,

        [parameter(ValueFromPipelineByPropertyName=$true)]
        [string]$ActionPreference = "Block&Alert"
        )

    begin
    {
        $VerbosePreference = "Continue"
        $CurrentScriptName = $MyInvocation.MyCommand.Name

        $Message = "<$CurrentScriptName>: Starting Script" 
        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
        Write-Verbose $Message

        $parallel = $False
    }

    process
    {
        if ($DecontaminationStagingArea -like "*$StagingAreaName")
        {
            $User_Staging_Area = Join-Path $DecontaminationStagingArea "$UserName\Unpacking_Area"
            $Direction = "FromUntrustToTrust"
            $File_Destination = Join-Path $TrustHomeFolder "$UserName\From-Untrust"
        }
        elseif ($DLPStagingArea -like "*$StagingAreaName")
        {
            $User_Staging_Area = Join-Path $DLPStagingArea "$UserName\Unpacking_Area"
            $Direction = "FromTrustToUntrust"
            $File_Destination = Join-Path $UnTrustHomeFolder "$UserName\From-Trust"
        }
        else
        {
            $Message = "<$CurrentScriptName>: Could not determine the user staging area"
            Write-EventLog -LogName Decon -source Decon -EntryType Error -EventId 2000 -Message $Message
            Write-Verbose $Message
            exit
        }

        $File_Hash = (Get-FileHash -Algorithm SHA256 -Path $FilePath).Hash

        $FileDBReports = @()
        if($FileDB.ContainsKey($File_Hash))
        {
            $Message = "<$CurrentScriptName>: FileDB has a file with this hash." 
            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
            Write-Verbose $Message

            # Retrieve all Detection Module ValidScanDurations for Detection Modules in the file's direction
            foreach ($Module in $DetectionModulesSettings.Keys)
            {
                if($DetectionModulesSettings.$Module.$Direction.ToLower() -eq "true")
                {
                    $Message = "<$CurrentScriptName>: Checking Module $Module's last scan date." 
                    Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
                    Write-Verbose $Message

                    $Duration = $DetectionModulesSettings.$Module.ValidCheckDuration.Duration             
                    $Unit = $DetectionModulesSettings.$Module.ValidCheckDuration.Unit
                    $TimeSpanArgs = @{$Unit = $Duration}
                    $TimeSpan = New-TimeSpan @TimeSpanArgs
                    $LastDateChecked = $FileDB.$File_Hash.$Module.DateChecked
                
                    if((New-Timespan -Start $LastDateChecked -End ([datetime]::Now)) -lt $TimeSpan)
                    {
                        $Message = "<$CurrentScriptName>: Module $Module's last scan date is within an acceptable timespan. Retrieving report." 
                        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
                        Write-Verbose $Message

                        $FileDBReports += @{"ModuleName" = $Module; "Hits" = $FileDB.$File_Hash.$Module.Hits}
                    }
                    else
                    {   
                        # This file needs to be re-scanned
                        # Breaking out of the statement, causes all Detection Modules to re scan the file
                        # this should probably just re-scan the file with the modules that are not in compliance
                        $Message = "<$CurrentScriptName>: Module $Module's last scan date is not within an acceptable timespan. Re-scanning file." 
                        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
                        Write-Verbose $Message

                        if ($FileDBReports)
                        {
                            Clear-Variable -Name $FileDBReports   
                        }
                        break
                    } 
                }      
            }
        }

        if($FileDBReports)
        {
            $Splat = @{
                'DetectionModulesSettings' = $DetectionModulesSettings;
                'ActionThreshHold' = $ActionThreshHold;
                'Reports' = $FileDBReports
            }
        
            # Call verdict passing function (Get-DeconReportsVerdict)
            $ThresholdMet = Get-DeconReportsVerdict @Splat
            
            $Splat = @{
                'Destination' = $File_Destination;
                'ThreshHoldMet' = $ThresholdMet;
                'FilePath' = $FilePath;
                'ActionPreference' = $ActionPreference;
                'QuarantineFolder' = $QuarantineFolder
            }
               
            Resolve-DeconVerdict @Splat
        }
        else
        {
            <#
            1.) Folder we will be unpacking this files contents into if it is unpackable
                ISSUE : This block is not working for DLP Unpacking folder for unknown reasons. 
                        (Works up to DLP\Staging\Original but never moves into unpacking area)
            
            2.) All the code in this else statement before Get-DeconReportsVerdict could be made
                into a function that handled just calling and returning reports to be analyzed.
                This would simplify debugging and changing the code in the future.
            #>

            $Message = "<$CurrentScriptName>: Scanning file." 
            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
            Write-Verbose $Message

            $Unpacking_Folder = Unpack-DeconFile -FilePath $FilePath -UserStagingArea $User_Staging_Area -UnpackingToolLocation $UnpackingToolLocation

            # Could not get the function to return the reports. Extracting code and adding it to this function to bypass problem. 
            # Major fail on my part but i need to move on.
            #$Reports = Start-DetectionModules -UnpackingFolder $Unpacking_Folder -DetectionModulesSettings $DetectionModulesSettings -Direction $Direction

            $Reports = @()
            $Sessions_and_Handles = @()
            foreach ($FILE in (Get-ChildItem -Path "$Unpacking_Folder\*" -Recurse | Where-Object -Property PSiscontainer -EQ $false))
            {   
                $Message = "<$CurrentScriptName>: File found in unpacking folder: $FILE" 
                Write-Verbose $Message
                foreach ($MODULE in $DetectionModulesSettings.Keys)
                {
                    $Message = "<$CurrentScriptName>: Found Configured Module in Decon Settings: $MODULE" 
                    Write-Verbose $Message
                    if ($DetectionModulesSettings.$MODULE.$Direction.ToLower() -eq "true")
                    {
                        $Message = "<$CurrentScriptName>: Module direction matches file direction: $Direction" 
                        Write-Verbose $Message
                        
                        $Message = "<$CurrentScriptName>: Decon configured to run in parallel: $parallel" 
                        Write-Verbose $Message
                    
                        if ($parallel)
                        {
                            $Scriptblock = {
                            Import-Module -Name $args[0]
                            & $args[1] -FilePath $args[2]
                            }

                            $Arguments = @($MODULE, $DetectionModulesSettings.$MODULE.InvocationCommand, $FILE.FullName)
                            
                            $Session = New-Session -RunspaceName $MODULE -Scriptblock $Scriptblock -Arguments $Arguments
                    
                            $Handle = $Session.BeginInvoke()
                            $Sessions_and_Handles += @{"Session" = $Session; "Handle" = $Handle}
                        }
                        else
                        {
                            Import-Module -Name $MODULE
                            $Reports += & $DetectionModulesSettings.$MODULE.InvocationCommand -FilePath $FILE.FullName
                        }
                   
                        $Message = "<$CurrentScriptName>: Import-Module -name $MODULE; " + $DetectionModulesSettings.$MODULE.InvocationCommand + " -FilePath" + $FILE.fullname 
                        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message $Message
                        Write-verbose $Message
                    }
                }
            }
            
            #This loop will Retrieve returns from Detection Modules running in seperate runspaces
            if ($parallel)
            {
                foreach($Session_and_Handle in $Sessions_and_Handles)
                {
                    $Message = "<$CurrentScriptName>: Starting sleep untill Detection Modules are finished." 
                    Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
                    Write-verbose $Message

                    while (-not($Session_and_Handle.Handle.IsCompleted -eq "True"))
                    {
                        Start-Sleep -Seconds 1
                    }

                    $Reports += $Session_and_Handle.Session.Endinvoke($Session_and_Handle.Handle)
                }
            }

            $Splat = @{
                'DetectionModulesSettings' = $DetectionModulesSettings;
                'ActionThreshHold' = $ActionThreshHold;
                'Reports' = $Reports
            }

            $ThresholdMet = Get-DeconReportsVerdict @Splat

            $Splat = @{
                'Destination' = $File_Destination;
                'ThreshHoldMet' = $ThresholdMet;
                'FilePath' = $FilePath;
                'UnpackingFolder' = $Unpacking_Folder;
                'ActionPreference' = $ActionPreference;
                'QuarantineFolder' = $QuarantineFolder
            }

            Resolve-DeconVerdict @Splat
            
            $Verdict = 'good'
            if ($ThresholdMet){

                $Verdict = 'bad'
            }

            Update-DeconFileDB -Report $Reports -FileDB ([ref]$FileDB) -Verdict $Verdict
        }
    }    
}