function New-Runspace() 
{
    Param(
    [Parameter (Mandatory=$True)]
    [string[]]$Runspace_Names
    )

    # New-Runspace creates one or more runspaces, given a name or list of names. 
    # It has one parameter, which can either be the name of one runspace, or a list of names of runspaces to be created.
    # It returns a pointer to the runspace, or a list of pointers to the runspaces.
    # NOTE: To substantiate Runspaces into current session, make sure these runspaces are returned to a variable
    # EX: $x = New-Runspace("My_Runspace")
    
    $List_of_Runspaces = @()
    foreach ($Runspace_Name in $Runspace_Names)
    {
        try
        {
            $New_Runspace = [runspacefactory]::CreateRunspace()
            $New_Runspace.Name = $Runspace_Name
            $List_of_Runspaces += $New_Runspace
        }
        catch
        {
            "ERROR: Unable to create Runspace, for a valid string was not given." | Out-Host 
            "Object Type: " + ($Runspace_Name.GetType()) + ", Object: " + ($Runspace_Name) | Out-Host
        }
    }
    return $List_of_Runspaces
}