﻿function Unpack-DeconFile()
{
    Param(
    [string]$FilePath,
    [string]$UserStagingArea,
    [string]$UnpackingToolLocation

    )
        
    $File_Name = $FilePath.split("\")[-1]
    $File_Hash = (Get-FileHash -Algorithm SHA256 -Path $FilePath).Hash
    $Unpacking_Folder_Name = "$File_Hash`_$File_Name"
    $Unpacking_Folder = Join-Path $UserStagingArea $Unpacking_Folder_Name
    $null = New-Item -Path $UserStagingArea -Name $Unpacking_Folder_Name -ItemType Directory -Force

    $null = Copy-Item -Path $FilePath -Destination $Unpacking_Folder -Force

    $null = Expand-ItemsInFolder -TargetFolder $Unpacking_Folder -OutPutFolder $Unpacking_Folder -UnpackingToolLocation $UnpackingToolLocation

    return $Unpacking_Folder
}