﻿function New-FileCacheKey()
{
    Param(
        [Parameter(Mandatory=$true)]
        [string]$SHA256,

        [Parameter(Mandatory=$true)]
        [int]$Hits,

        [Parameter(Mandatory=$true)]
        [string]$ReportDate,

        [Parameter(Mandatory=$true)]
        [string]$ModuleName,

        [Parameter(Mandatory=$true)]
        [hashtable]$FileCache
    )

    $FileCache.add($SHA256, @{})
    $FileCache.$SHA256.add($ModuleName, @{})
    $FileCache.$SHA256.$ModuleName.Add("Hits", $Hits)
    $FileCache.$SHA256.$ModuleName.Add("ReportDate", $ReportDate)
}