﻿function Start-DetectionModules()
{
    Param(
    [parameter(Mandatory=$true, 
                ValueFromPipelineByPropertyName=$true)]
    [string]$UnpackingFolder,
            
    [parameter(Mandatory=$true, 
                ValueFromPipelineByPropertyName=$true)]
    [hashtable]$DetectionModulesSettings,
            
    [parameter(Mandatory=$true, 
                ValueFromPipelineByPropertyName=$true)]
    [string]$Direction
    )

    begin
    {
        $VerbosePreference = "Continue"
        $CurrentScriptName = $MyInvocation.MyCommand.Name
        $Reports = @()
        $Sessions_and_Handles = @()
    }
    
    process
    {
        foreach ($FILE in (Get-ChildItem -Path "$UnpackingFolder\*"))
        {   
            
            foreach ($MODULE in $DetectionModulesSettings.Keys)
            {
            
                if ($DetectionModulesSettings.$MODULE.$Direction.ToLower() -eq "true")
                {
                    $Scriptblock = {
                        Import-Module -Name $args[0]
                        & $args[1] -FilePath $args[2]
                    }

                    $Arguments = @($MODULE, $DetectionModulesSettings.$MODULE.InvocationCommand, $FILE.FullName)

                    $parallel = $false
                    if ($parallel)
                    {
                        $Session = New-Session -RunspaceName $MODULE -Scriptblock $Scriptblock -Arguments $Arguments
                    
                        $Handle = $Session.BeginInvoke()
                    }
                    else
                    {
                        Import-Module -Name $MODULE
                        $Reports += & $DetectionModulesSettings.$MODULE.InvocationCommand -FilePath $FILE.FullName
                    }
                   
                    $Message = "<$CurrentScriptName>: Import-Module -name $MODULE; $InvocationCommand -FilePath" + $FILE.fullname 
                    Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message $Message
                    Write-verbose $Message
                
                    $Sessions_and_Handles += @{"Session" = $Session; "Handle" = $Handle}
                }
            }
        }
    
        #This loop will Retrieve returns from Detection Modules running in seperate runspaces
        if ($parallel)
        {
            foreach($Session_and_Handle in $Sessions_and_Handles)
            {
                $Message = "<$CurrentScriptName>: Starting sleep untill Detection Modules are finished." 
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
                Write-verbose $Message

                while (-not($Session_and_Handle.Handle.IsCompleted -eq "True"))
                {
                    Start-Sleep -Seconds 1
                }

                $Reports += $Session_and_Handle.Session.Endinvoke($Session_and_Handle.Handle)
            }
        }
    }

    end
    {
        Out-GridView -InputObject $Reports #Start-DetectionModules.ps1
        
        return $Reports
    }
}