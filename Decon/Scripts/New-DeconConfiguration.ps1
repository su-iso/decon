﻿function New-DeconConfiguration()
{
    $x = New-Object -TypeName PSObject
    
    $x | Add-Member -Name Settings -MemberType NoteProperty -Value (New-Object -TypeName PSObject)
    
    $x | Add-Member -Name APIKeys -MemberType NoteProperty -Value (New-Object -TypeName PSObject)
    
    $x | Add-Member -Name DetectionModules -MemberType NoteProperty -Value (New-Object -TypeName PSObject)
    
    $x.Settings | Add-Member -Name LogLocation -MemberType NoteProperty -Value "C:\DeconProject\Logs\Decon_Master_Log.txt"
    $x.Settings | Add-Member -Name UserConfiguration -MemberType NoteProperty -Value "C:\DeconProject\Config\PAW_Users.txt"
    $x.Settings | Add-Member -Name DeconHomeFolders -MemberType NoteProperty -Value "C:\DeconProject\Decon\home"
    $x.Settings | Add-Member -Name CIFSHomeFolders -MemberType NoteProperty -Value "PAW_Untrust_Folder:\\Users"
    $x.Settings | Add-Member -Name DecontaminationStagingArea -MemberType NoteProperty -Value "C:\DeconProject\Decon\Staging\Decontamination"
    $x.Settings | Add-Member -Name DLPStagingArea -MemberType NoteProperty -Value "C:\DeconProject\Decon\Staging\DLP"
    $x.Settings | Add-Member -Name CIFSServerCredLocation -MemberType NoteProperty -Value "C:\DeconProject\Config\CLICredObject.xml"

    $x.APIKeys | Add-Member -Name VirusTotal -MemberType NoteProperty -Value "e6f5d0524fe55290251482a429aad8ef1785a6ef9bd3ffe99e5180d205865198"

    $x.DetectionModules | Add-Member -Name VirusTotal -MemberType NoteProperty -Value (New-Object -TypeName PSObject)
    
    $x.DetectionModules.VirusTotal | Add-Member -Name ModuleLocation -MemberType NoteProperty -Value "C:\DeconProject\Detection_Modules\Virus_Total_Detection_Module.ps1"
    $x.DetectionModules.VirusTotal | Add-Member -Name Paramaters -MemberType NoteProperty -Value @("APIKey","FilePath","Hash")

    $x | ConvertTo-Json -Depth 10 | out-file -FilePath C:\DeconProject\Config\Decon_Configuration.json -Force
}


