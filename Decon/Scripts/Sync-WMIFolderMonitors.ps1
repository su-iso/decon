﻿function Sync-WMIFolderMonitors()
{
    [CmdletBinding()]

    Param(
    [int]$Interval = 10,
    [string]$DecontaminationStagingArea,
    [string]$DLPStagingArea,
    [string[]]$Users
    )
    
    Begin
    {
        $Users_With_Decon_File_Monitor = (Get-EventSubscriber -SourceIdentifier *_Decon_WMI_Folder_Monitor | ForEach-Object {$_.sourceidentifier.split("_")[0]})
        $Users_With_DLP_File_Monitor = (Get-EventSubscriber -SourceIdentifier *_DLP_WMI_Folder_Monitor | ForEach-Object {$_.sourceidentifier.split("_")[0]})
    }

    Process
    {
        # Check to make sure all users passed in to the function have a filemonitor in place
        Foreach($User in $Users)
        {
            if(-not($User -in $Users_With_Decon_File_Monitor))
            {
                $Path = "$DecontaminationStagingArea\$User\Original"
                Register-WMIFolderMonitor -Path $Path -SourceIdentifier ($User + "_Decon_WMI_Folder_Monitor") -Interval $Interval
                
                $Message = "Registering `"$User`" Decon folder monitor"
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
            }

            if(-not($User -in $Users_With_DLP_File_Monitor))
            {
                $Path = "$DLPStagingArea\$User\Original"
                Register-WMIFolderMonitor -Path $Path -SourceIdentifier ($User + "_DLP_WMI_Folder_Monitor") -Interval $Interval
                
                $Message = "Registering `"$User`" DLP folder monitor"
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
            }
        }

        # Check to see if there are user with a file monitor in place that where not passed into the function.
        # If so then delete them.
        Foreach($User in $Users_With_Decon_File_Monitor)
        {
            if(-not($User -in $Users))
            {
                Unregister-Event -SourceIdentifier "$User`_Decon_WMI_Folder_Monitor"
                
                $Message = "Removing `"$User`" Decon folder monitor"
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
            }
        }

        Foreach($User in $Users_With_DLP_File_Monitor)
        {
            if(-not($User -in $Users))
            {
                Unregister-Event -SourceIdentifier "$User`_DLP_WMI_Folder_Monitor"
                
                $Message = "Removing `"$User`" DLP folder monitor"
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
            }
        }
    }
}