﻿function Get-DeconReportsVerdict()
{
    Param(
        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [hashtable] $DetectionModulesSettings,

        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [string]$ActionThreshHold,

        [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
        [hashtable[]]$Reports
    )

    # This loop looks at all the reports returned and checks them against thew threshhold of the module that scanned them
    # If any of the reports exceed the threshhold then they set threshhold to $true. The threshhold can cause alerts
    # and blocks to accure for the user in DMM.
    $Report_Summary = @{}
    foreach($Report in $Reports)
    {
        $Module = $Report.ModuleName
        
        if($Report.hits -ge $DetectionModulesSettings.$module.HitsThreshhold)
        {
            $Report_Summary.$Module = $True    
        }
        else
        {
            $Report_Summary.$Module = $False
        }
    }

    if ($Report_Summary.Values.where({$_ -eq $true}).count -ge $ActionThreshHold)
    {
        $ThresholdMet = $True
    }
    else
    {
        $ThresholdMet = $False
    }

    return $ThresholdMet
}