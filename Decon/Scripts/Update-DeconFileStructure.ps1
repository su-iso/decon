﻿function Update-DeconFileStructure()
{
    param(
    [string]$TrustHomeDirectory,
    [string]$UntrustHomeDirectory,
    [string]$DecontaminationStagingArea,
    [string]$DLPStagingArea,
    [string]$QuarantineFolder,
    [string[]]$Users
    )

    # This section checks for user folders on Decon that are not listed in the users file.
    # When it finds one it removes folders with the same user name from all location related to the Decon file structure
    
    $HomeDirectories = (Get-ChildItem -Path $TrustHomeDirectory) + (Get-ChildItem -Path $UntrustHomeDirectory)

    # Attempts to remove folders and files for users that were not in the users list.
    foreach ($folder in $HomeDirectories)
    {
      if (-not($folder.Name -in $Users))
      {
        Remove-Item -path (join-path $TrustHomeDirectory $folder.Name) -Recurse -Force 
        Remove-Item -path (join-path $DecontaminationStagingArea $folder.Name) -Recurse -Force 
        Remove-Item -path (join-path $DLPStagingArea $folder.Name) -Recurse -Force 
        Remove-Item -path (join-path $UntrustHomeDirectory $folder.Name) -Recurse -Force
      }
    }

    # setup and repair any damaged folder strutures in the Decon file structure
    foreach ($User in $Users)
    {
        # Check if Trusted Home folders exist
        if (-not(test-path $TrustHomeDirectory\$User))
        {
            New-Item -path $TrustHomeDirectory -Name $User -ItemType Directory
            New-Item -path (join-path $TrustHomeDirectory $User) -Name To-Untrust -ItemType Directory
            New-Item -path (join-path $TrustHomeDirectory $User) -Name From-Untrust -ItemType Directory
        }   

        if (-not(test-path $TrustHomeDirectory\$User\To-Untrust))
        {
            New-Item -path (join-path $TrustHomeDirectory $User) -Name To-Untrust -ItemType Directory
        }   

        if (-not(test-path $TrustHomeDirectory\$User\From-Untrust))
        {
            New-Item -path (join-path $DeconHomeDirectory $User) -Name From-Untrust -ItemType Directory
        }

        # Check if CIFS folder exist
        if (-not(test-path (join-path $UntrustHomeDirectory $User)))
        {
            New-Item -Path $UntrustHomeDirectory -Name $User -ItemType Directory
            New-Item -path (join-path $UntrustHomeDirectory $User) -Name To-Trust -ItemType Directory
            New-Item -path (join-path $UntrustHomeDirectory $User) -Name From-Trust -ItemType Directory
        }

        if (-not(test-path $UntrustHomeDirectory\$User\To-Trust))
        {
            New-Item -path (join-path $UntrustHomeDirectory $User) -Name To-Trust -ItemType Directory
        }   

        if (-not(test-path $UntrustHomeDirectory\$User\From-Trust))
        {
            New-Item -path (join-path $UntrustHomeDirectory $User) -Name From-Trust -ItemType Directory
        }

        # Check if Decontamination Folder exists
        if (-not(test-path (join-path $DecontaminationStagingArea $User)))
        {
            New-Item -Path $DecontaminationStagingArea -Name $User -ItemType Directory
            New-Item -path (join-path $DecontaminationStagingArea $User) -Name Original -ItemType Directory
            New-Item -path (join-path $DecontaminationStagingArea $User) -Name Unpacking_Area -ItemType Directory
        }

        if (-not(test-path $DecontaminationStagingArea\$User\Original))
        {
            New-Item -path (join-path $DecontaminationStagingArea $User) -Name Original -ItemType Directory
        }   

        if (-not(test-path $DecontaminationStagingArea\$User\Unpacking_Area))
        {
            New-Item -path (join-path $DecontaminationStagingArea $User) -Name Unpacking_Area -ItemType Directory
        }

        # Check if DLP folders exist
        if (-not(test-path (join-path $DLPStagingArea $User)))
        {
            New-Item -Path $DLPStagingArea -Name $User -ItemType Directory
            New-Item -path (join-path $DLPStagingArea $User) -Name Original -ItemType Directory
            New-Item -path (join-path $DLPStagingArea $User) -Name Unpacking_Area -ItemType Directory
        }

        if (-not(test-path $DLPStagingArea\$User\Original))
        {
            New-Item -path (join-path $DLPStagingArea $User) -Name Original -ItemType Directory
        }   

        if (-not(test-path $DLPStagingArea\$User\Unpacking_Area))
        {
            New-Item -path (join-path $DLPStagingArea $User) -Name Unpacking_Area -ItemType Directory
        }
    }

    # Check if Quarantine folders exist
    if (-not(test-path $QuarantineFolder))
    {
        $Path = $QuarantineFolder | Split-Path -Parent
        $Name = $QuarantineFolder | Split-Path -Leaf
        New-Item -path $Path -Name $Name -ItemType Directory -force
    }
}