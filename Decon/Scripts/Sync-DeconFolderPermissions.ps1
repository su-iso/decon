﻿function Sync-DeconFolderPermissions()
{
    Param(
    [Parameter(Mandatory=$true)]
    [string]$UserName,

    [Parameter(Mandatory=$true)]
    [string[]]$UserTrustAccounts,

    [Parameter(Mandatory=$true)]
    [string[]]$UserUntrustAccounts,

    [Parameter(Mandatory=$true)]
    [string]$UntrustHomeFolder,

    [Parameter(Mandatory=$true)]
    [string]$TrustHomeFolder,

    [Parameter(Mandatory=$false)]
    [string[]]$ExpectedUserPermissions = @("Read","Write","Delete","Synchronize")
    )

    $FolderInformation = @(@{HomeFolder = $UntrustHomeFolder; Accounts = $UserUntrustAccounts},@{HomeFolder = $TrustHomeFolder; Accounts = $UserTrustAccounts})

    foreach ($Folder in $FolderInformation)
    {
        $Message = "Checking " + $Folder.HomeFolder + " permissions" 
        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
        
        # Create a list of approved accounts that should have NTFS permission on the users Untrust folder
        $Approved_Accounts = @((Get-NTFSAccess -Path $Folder.HomeFolder).Account.AccountName)
        $Approved_Accounts += $Folder.Accounts
        $User_Folder = (Join-Path $Folder.HomeFolder $UserName)
    
        # Get all the accounts that have permission on the Users Folder
        # and check to see if they are in the list of approved accounts.
        # If they are NOT then remove the account's rights from the folder.
        # Else if it is a users account then check to see that it has the
        # permissions that you are expecting. Remove any unexpected permissions
        foreach ($Account in (Get-NTFSAccess -Path $User_Folder).Account.AccountName)
        {
            if (-not($Account -in $Approved_Accounts))
            {
                $Permissions = @(Get-NTFSAccess -Path $User_Folder -Account $Account).AccessRights
                Remove-NTFSAccess -Path $User_Folder -Account $Account -AccessRights $Permissions
                $Message = "Unexpected Permissions: Removing $Permissions from $User_Folder for $Account"  
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
                
                $Message = "Remove-NTFSAccess -Path $User_Folder -Account $Account -AccessRights $Permissions"  
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
            }
            elseif ($Account -in $Folder.Accounts)
            {
                
                $Permissions = (Get-NTFSAccess -Path $User_Folder -Account $Account).AccessRights.ToString().split(',').Trim()
                foreach ($Permission in $Permissions)
                {
                    if (-not($Permission -in $ExpectedUserPermissions))
                    {
                        Remove-NTFSAccess -Path $User_Folder -Account $Account -AccessRights $Permission
                        $Message = "Unexpected Permissions: Removing $Permissions from $User_Folder for $Account"  
                        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
                        
                        $Message = "Remove-NTFSAccess -Path $User_Folder -Account $Account -AccessRights $Permissions"  
                        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
                    }
                }

                $Permissions = (Get-NTFSAccess -Path $User_Folder -Account $Account).AccessRights.ToString().split(',').Trim()
                $Unexpected_Permissions = Compare-Object -ReferenceObject $Permissions -DifferenceObject $ExpectedUserPermissions
                if ($Unexpected_Permissions)
                {
                    Add-NTFSAccess -Path $User_Folder -Account $Account -AccessRights $ExpectedUserPermissions
                    $Message = "Unexpected Permissions: Adding $ExpectedUserPermissions to $User_Folder for $Account"
                    Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
                    
                    $Message = "Add-NTFSAccess -Path $User_Folder -Account $Account -AccessRights $ExpectedUserPermissions"   
                    Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
                }
            }
        }

        $FolderAccounts = (Get-NTFSAccess -Path $User_Folder).Account.AccountName.Trim()
        $Difference = Compare-Object -ReferenceObject $Folder.Accounts -DifferenceObject $FolderAccounts
        $Missing_Accounts = @($Difference | Where-Object {$_.SideIndicator -eq "<="} | Select-Object -ExpandProperty InputObject)
        
        foreach ($Account in $Missing_Accounts)
        {
            Add-NTFSAccess -Path $User_Folder -Account $Account -AccessRights $ExpectedUserPermissions 
            $Message = "User Missing: Adding $Account to $User_Folder with Permissions: $ExpectedUserPermissions"
            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
             
            $Message = "Add-NTFSAccess -Path $User_Folder -Account $Account -AccessRights $ExpectedUserPermissions"   
            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
        }
    }
}