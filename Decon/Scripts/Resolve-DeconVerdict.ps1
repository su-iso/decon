﻿function Resolve-DeconVerdict()
{
    Param(
    [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
    [boolean]$ThreshHoldMet,

    [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
    [string]$Destination,
    
    [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
    [string]$FilePath,
    
    [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
    [string]$QuarantineFolder,
    
    [parameter(ValueFromPipelineByPropertyName=$true)]
    [string]$UnpackingFolder,
    
    [parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)]
    [string]$ActionPreference
    )
   
    $CurrentScriptName = $MyInvocation.MyCommand.Name

    if ($ThresholdMet -eq $False)
    {
        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message "<$CurrentScriptName>: Action Threshold not met. Continuing file transfer. "
        Copy-Item -Path $FilePath -Destination $Destination
        Remove-Item -Path $FilePath -Force

        if ($UnpackingFolder)
        {
            Remove-Item -Path $UnpackingFolder -Force -Recurse 
        }
    }
    elseif ($ThresholdMet -eq $True)
    {
        # PositiveActionPreference refers to the actions taken when a report is unclean. 
        # It should be a configuration file variable that specifies wether the files should be 
        # allowed to Trusted Systems.

        if($ActionPreference.ToLower() -eq "block&alert")
        {
            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message "<$CurrentScriptName>: Action Threshold Met. Blocking files and alerting. "    
        }
        elseif($ActionPreference.ToLower() -eq "allow&alert")
        {
            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message "<$CurrentScriptName>: Action Threshold Met. Allowing File transfer and alerting. "
            Copy-Item -Path $FilePath -Destination $Destination -Force
            
        }
        
        Copy-Item -Path $FilePath -Destination $QuarantineFolder -Force
        Remove-Item -Path $FilePath -Force

        if ($UnpackingFolder)
        {
            Remove-Item -Path $UnpackingFolder -Force -Recurse
        }
    }
    else
    {
        Write-EventLog -LogName Decon -source Decon -EntryType Error -EventId 2000 -Message "<$CurrentScriptName>: Errored out passing verdict on report."
    }
}