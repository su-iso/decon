﻿function ConvertFrom-JsonCustomObject()
{
    Param(
    [Parameter(Mandatory=$true, 
               ValueFromPipeline=$true,
               ValueFromPipelineByPropertyName=$true, 
               Position=0)]
    [PSCustomObject]
    $JsonObject
    )

    function Test-ObjectType()
    {
        Param(
            $JsonObject,
            $Member,
            $Hashtable
        )

        if ($JsonObject.$Member -is [pscustomobject])
        {
            $Nested_Hashtable = @{}
            $Hashtable.Add($Member, $Nested_Hashtable)
        
            foreach($Nested_Member in ($JsonObject.$Member | Get-Member -MemberType NoteProperty).name)
            {
                Test-ObjectType -JsonObject $JsonObject.$Member -Member $Nested_Member -Hashtable $Nested_Hashtable
            }
        }
        elseif($JsonObject.$Member -is [string])
        {
            $Hashtable.add($Member, $JsonObject.$Member)
        }
        elseif($JsonObject.$Member -is [Object[]])
        {
            $Hashtable.add($Member, $JsonObject.$Member)
        }
    }

    $Hashtable = @{}
    foreach ($Member in ($JsonObject | Get-Member -MemberType NoteProperty).name)
    {
        Test-ObjectType -JsonObject $JsonObject -Member $Member -Hashtable $Hashtable
    }
    $Hashtable
}