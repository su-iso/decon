﻿function Update-DeconFileDB()
{
    Param(
    [parameter(Mandatory=$true, 
               ValueFromPipelineByPropertyName=$true)]
    $Report,

    [parameter(Mandatory=$true, 
               ValueFromPipelineByPropertyName=$true)]
    [hashtable]$FileDB,

    [parameter(Mandatory=$true, 
               ValueFromPipelineByPropertyName=$true)]
    [validateset("good","bad")]
    [string]$Verdict
    )

    foreach ($Report in $Reports)
    {
        $Hash = $Report.Hash
        $ModuleName = $Report.ModuleName

        if (-not($FileDB.ContainsKey($Report.Hash)))
        {
                $FileDB.add($Hash, @{})
                $FileDB.$Hash.add("Verdict", $null)
                $FileDB.$Hash.add("VerdictDate", $null)
        }

        if(-not($FileDB.$Hash.ContainsKey($ModuleName)))
        {
            $FileDB.$Hash.add($ModuleName, @{})
            $FileDB.$Hash.$ModuleName.add("Hits", $null)
            $FileDB.$Hash.$ModuleName.add("DateChecked", $null)
        }
            
        $FileDB.$Hash.Verdict = $Verdict
        $FileDB.$Hash.VerdictDate = [datetime]::Now
        $FileDB.$Hash.$ModuleName.Hits = $Report.Hits
        $FileDB.$Hash.$ModuleName.DateChecked = [datetime]::Now
        # Log this activity
    }
}