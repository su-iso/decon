﻿function Expand-ItemsInFolder()
{
    Param(
    [Parameter(Mandatory=$True)]
    [string]$TargetFolder,

    [string]$OutPutFolder = $TargetFolder,

    [string]$UnpackingToolLocation = "$env:ProgramFiles\Z-zip\7z.exe"
    )

    $CurrentScriptName = $MyInvocation.MyCommand.Name

    # recurse through the potentially zipped file till we no longer get new items
    # do not extract files that have already been extracted (-aos)
    $New_Files = $true
    while ($New_Files)
    {
        $Current_File_Count = @(Get-ChildItem -Path $TargetFolder\*).Count
        
        Start-Process -FilePath "$UnpackingToolLocation" `
                      -ArgumentList "x $TargetFolder\* -r -y -o`"$OutPutFolder`" -aos" `
                      -NoNewWindow `
                      -PassThru `
                      -Wait
        
        $New_File_Count = @(Get-ChildItem -Path $TargetFolder\*).Count
        $New_Files = ($New_File_Count -gt $Current_File_Count)

        $Message = "<$CurrentScriptName>: Start-Process -FilePath $UnpackingToolLocation e $TargetFolder\* -r -y -o $OutPutFolder -aos -NoNewWindow -PassThru -Wait"
        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
    }
}   