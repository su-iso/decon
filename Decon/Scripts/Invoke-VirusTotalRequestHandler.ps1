﻿function Invoke-VirusTotalRequestHandler()
{
    Param (
    
        [Parameter(Mandatory=$true)]
        [string]$RunspaceName,

        [Parameter(Mandatory=$true)]
        [string]$ModuleName,

        [Parameter(Mandatory=$true)]
        [string[]]$APIKeys
    )

    $CurrentScriptName = $MyInvocation.MyCommand.Name
    $Message = "<$CurrentScriptName>: Starting Script" 
    Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message

    $scriptblock = {
    Import-module -Name $args[0]
    VirusTotalRequestHandler -VTAPIKeyList $args[1]
    }

    $VTRHSession = New-Session -RunspaceName $RunspaceName -Scriptblock $Scriptblock -Arguments @($ModuleName, $APIKeys)

    $Handle = $VTRHSession.BeginInvoke()
    
    return $VTRHSession
}