﻿function Register-WMIFolderMonitor()
{
    Param(
    [parameter(mandatory=$true)]
    $Path,
    [parameter(mandatory=$true)]
    $SourceIdentifier,
    [int]$Interval = 10
    )

    $WMIDrive = $Path | split-path -Qualifier
    $WMIPath = ($Path | split-path -NoQualifier).Replace("\","\\") + "\\"
    
    $Query = "SELECT * FROM __InstanceCreationEvent WITHIN $Interval WHERE targetInstance ISA 'CIM_DataFile' AND targetInstance.Drive = `"$WMIDrive`" AND targetInstance.Path = `"$WMIPath`""

    Register-WmiEvent -Query $Query -SourceIdentifier "$SourceIdentifier"

    $Message = "<$CurrentScriptName>: Register-WmiEvent -Query $Query -SourceIdentifier $SourceIdentifier"
    Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message $Message
}