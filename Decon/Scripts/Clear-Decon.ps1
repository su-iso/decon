﻿function Clear-Decon()
{

    Param(

    [string]$Username = $Env:USERNAME
    
    )
    
    remove-job -Name *
    remove-event -SourceIdentifier *
    Unregister-Event -SourceIdentifier *

    Close-Runspace -RunspaceNames @("VirusTotalDetectionModule", "DetectionModuleManager")
    Dispose-Runspace -RunspaceNames @("VirusTotalDetectionModule", "DetectionModuleManager")

    remove-item -Path C:\Staging\Decontamination\$Username\Original\* -Force -Recurse
    remove-item -Path C:\Staging\Decontamination\$Username\Unpacking_Area\* -Force -Recurse
    remove-item -Path C:\Staging\DLP\$Username\Original\* -Force -Recurse
    remove-item -Path C:\Staging\DLP\$Username\Unpacking_Area\* -Force -Recurse
    remove-item -Path C:\Trust\Home\$Username\From-Untrust\* -Force -Recurse
    remove-item -Path C:\Untrust\Home\$Username\From-Trust\* -Force -Recurse
    remove-item -Path C:\Users\$Username\Documents\WindowsPowerShell\Modules\Decon\Config\DeconDB.xml -Force
    remove-module -name Decon
    import-module -name Decon
}