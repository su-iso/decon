﻿function New-CredentialObject
{
    Param
    (
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true)]
                   [string]
                   $UserInformationFile
    )
    
    $UserName = (Get-Content $UserInformationFile).split("`n")[0].split(',')[0].TrimEnd()
    $Password = (Get-Content $UserInformationFile).split("`n")[0].split(',')[1].TrimEnd() | convertto-securestring -AsPlainText -Force
    $cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $UserName, $Password
    return $cred
}