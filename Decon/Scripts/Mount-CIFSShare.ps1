﻿function Mount-CIFSShare()
{
  Param(
    $LocalShareName,
    $PSProvider = 'FileSystem',
    $RemoteShareName,
    $Description = 'UntrustHomeFolder',
    $Credential
  )

  # You must be on the VPN to make this work until the code is running on the Decon Server.
  # This takes some time. Kerberos must be slowing it down dramatically.
  if (-not(Get-PSDrive -Name $LocalShareName -ErrorAction SilentlyContinue))
  {
    $null = New-PSDrive -Name $LocalShareName -PSProvider $PSProvider -Root $RemoteShareName -Description $Description -Credential $cred
  }
}