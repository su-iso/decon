﻿function New-Session()
{
    Param(
    [Parameter (Mandatory=$False)]
    [string] $RunspaceName,
    
    [Parameter (Mandatory=$False)]
    [string] $Scriptblock,
    
    [Parameter (Mandatory=$False)]
    [Object[]] $Arguments
    )

    # New-Session creates a new powershell session, and returns a pointer to the session.
    # Optional Parameters : 
    #     $RunspaceName - A string to name the runspace of the session
    #     $Scriptblock - A scriptblock to add to the session
    #     $Arguments - Arguments to add to the scripblock 

    $CurrentScriptName = $MyInvocation.MyCommand.Name

    $Session = [powershell]::Create()
    
    if(-not($RunspaceName -eq $Null))
    {
        $Session.Runspace.Name = $RunspaceName
    }

    if(-not($Scriptblock -eq $Null))
    {
        $Null = $Session.AddScript($Scriptblock)
        
        if(-not($Arguments -eq $Null))
        {
            foreach($Argument in $Arguments)
            {
                $Null = $Session.AddArgument($Argument)
            }
        }
    }
    return $Session
}