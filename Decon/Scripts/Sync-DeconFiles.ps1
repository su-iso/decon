﻿function Sync-DeconFiles()
{
    param(
    [parameter(Mandatory=$true)]
    [string]$TrustHomeDirectory,

    [parameter(Mandatory=$true)]
    [string]$UntrustHomeDirectory,

    [parameter(Mandatory=$true)]
    [string]$DecontaminationStagingArea,

    [parameter(Mandatory=$true)]
    [string]$DLPStagingArea,

    [parameter(Mandatory=$true)]
    [hashtable]$FileDB,

    [parameter(Mandatory=$true)]
    [string]$ActionPreference
    )

    $CurrentScriptName = $MyInvocation.MyCommand.Name
    $VerbosePreference = "Continue"
    
    $Files_In_ToUntrust = (Get-childitem -path ($TrustHomeDirectory + "\*\To-Untrust\*") -Recurse | Where-Object -Property PSIsContainer -EQ $False).FullName
    $Files_In_FromUnTrust = (Get-childitem -path ($TrustHomeDirectory + "\*\From-Untrust\*") -Recurse | Where-Object -Property PSIsContainer -EQ $False).FullName

    $Files_In_ToTrust = (Get-childitem -path ($UntrustHomeDirectory + "\*\To-Trust\*") -Recurse | Where-Object -Property PSIsContainer -EQ $False).FullName
    $Files_In_FromTrust = (Get-childitem -path ($UntrustHomeDirectory + "\*\From-Trust\*") -Recurse | Where-Object -Property PSIsContainer -EQ $False).FullName

    $Files_In_DLP_Original = (Get-ChildItem -path ($DLPStagingArea + "\*\Original") -Recurse | Where-Object -Property PSIsContainer -EQ $False).FullName
    $Files_In_Decontamination_Original = (Get-ChildItem -Path ($DecontaminationStagingArea + "\*\Original") -Recurse | Where-Object -Property PSIsContainer -EQ $False).FullName

    # Check if any files in To-Untrust need to be transfered into the Original folder for processing
    foreach ($FILE in $Files_In_ToUntrust)
    {
        $Username = ($FILE -split ($TrustHomeDirectory + "\"), 0, "simplematch")[1].split("\")[0].trim()
        $File_Name = $FILE.split('\')[-1]
        $File_Hash = (Get-FileHash -Path $FILE -Algorithm SHA256).hash
        
        $Untrust_File = "$UntrustHomeDirectory\$Username\From-Trust\$File_Name"
        $DLP_File = "$DLPStagingArea\$Username\Original\$File_Name"

        if(-not($Untrust_File -in $Files_In_FromTrust) -and -not($DLP_File -in $Files_In_DLP_Original))
        {
            if ($FileDB.ContainsKey($File_Hash))
            {
                if (-not($FileDB.$File_Hash.Verdict -eq 'bad' -and $ActionPreference -eq 'Block&Alert'))
                {
                    Copy-Item -Path $FILE -Destination $DLP_File -Force
                    Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message "<$CurrentScriptName>: Copy-Item -Path $FILE -Destination $DLP_File"
                }
            }
            else
            {
                Copy-Item -Path $FILE -Destination $DLP_File -Force
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message "<$CurrentScriptName>: Copy-Item -Path $FILE -Destination $DLP_File"
            }
        }
    }

    # Check if any files in To-Trust need to be transfered into the Original folder for processing
    foreach ($FILE in $Files_In_ToTrust)
    {
        $Username = ($FILE -split ($UntrustHomeDirectory + "\"), 0, "simplematch")[1].split("\")[0].trim()
        $File_Name = $FILE.split('\')[-1]
        $File_Hash = (Get-FileHash -Path $FILE -Algorithm SHA256).hash
        
        $Trusted_File = "$TrustHomeDirectory\$Username\From-Untrust\$File_Name"
        $Decontamination_File = "$DecontaminationStagingArea\$Username\Original\$File_Name"

        if(-not($Trusted_File -in $Files_In_FromUnTrust) -and -not($Decontamination_File -in $Files_In_Decontamination_Original))
        {
            if ($FileDB.ContainsKey($File_Hash))
            {
                if (-not($FileDB.$File_Hash.Verdict -eq 'bad' -and $ActionPreference -eq 'Block&Alert'))
                {
                    Copy-Item -Path $FILE -Destination $Decontamination_File -Force
                    Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message "<$CurrentScriptName>: Copy-Item -Path $FILE -Destination $Decontamination_File"
                }
            }
            else
            {
                Copy-Item -Path $FILE -Destination $Decontamination_File -Force
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message "<$CurrentScriptName>: Copy-Item -Path $FILE -Destination $Decontamination_File"
            }
        }
    }
}
