﻿function Dispose-Runspace()
{
    Param(
    [Parameter (Mandatory=$True)]
    [string[]]$RunspaceNames
    )

    # Dispose-Runspace takes one or more names of Runspaces, and disposes them.
    # Works with wildcards.
    
    foreach ($RunspaceName in $RunspaceNames)
    {
        try
        {
            $Disposed_Runspaces = Get-Runspace -Name $RunspaceName
            $Disposed_Runspaces.Dispose()    
        }
        catch
        {
            "ERROR: Unable to dispose of object, for it is not a valid Runspace Name." | Out-Host 
            "Object Type: " + ($RunspaceName.GetType()) + ", Object: " + ($RunspaceName) | Out-Host 
        }
    }
    $Disposed_Runspaces
}