﻿#Check for Decon Win Logs 

if(-not([System.Diagnostics.EventLog]::Exists('Decon/Operational')))
{
    New-EventLog -LogName Decon/Operational -Source Decon_Operational
}

if(-not([System.Diagnostics.EventLog]::Exists('Decon/Maintenance')))
{
    New-EventLog -LogName Decon/Maintenance -Source Decon_Maintenance
}

if(-not([System.Diagnostics.EventLog]::Exists('Decon/Diagnostic')))
{
    New-EventLog -LogName Decon/Diagnostic -Source Decon
}

