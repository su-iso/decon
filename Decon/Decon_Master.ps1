Param(

$ConfigurationPath = "$PSScriptRoot\Config\Decon_Configuration.json"

)

begin
{
    $CurrentScriptName = $MyInvocation.MyCommand.Name
    $Message = "<$CurrentScriptName>: Application Started"
    Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
    write-verbose $Message
    
    $Host.Runspace.name = "Decon_Master"
    
    if (test-path $PSScriptRoot\Config\DeconDB.xml)
    {
        $FileDB = Import-Clixml -Path "$PSScriptRoot\Config\DeconDB.xml"
    }
    else
    {
        $FileDB = [hashtable]::new()
        Export-Clixml -Path "$PSScriptRoot\Config\DeconDB.xml" -InputObject $FileDB -Force
    }

    $VerbosePreference = "Continue"
    $parallel = $true
}

process
{
    while ($true)
    {
        # Get the configuration files for the application
        $Decon_Master_Configuration = Get-Content -path $ConfigurationPath -raw | convertfrom-json | convertfrom-JsonCustomObject
        $Decon_User_Configuration = Get-Content -Path $Decon_Master_Configuration.Settings.UserConfiguration -raw | convertfrom-json | ConvertFrom-JsonCustomObject
        $Users = $Decon_User_Configuration.Users.keys

        $Message = "<$CurrentScriptName>: Starting Health Checks"
        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
        write-verbose $Message
    
        <#
        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message "<$CurrentScriptName>: Checking if VirusTotalRequestHandler is running"
        $runspacename = "VirusTotalRequestHandler"
        if (-not($runspacename -in (Get-Runspace).name))
        {
            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message "<$CurrentScriptName>: VirusTotalRequestHandler was not running. Invoking VirusTotalRequestHandler"
        
            $runspace = Invoke-VirusTotalRequestHandler -RunspaceName $runspacename `
                                                        -ModuleName $runspacename `
                                                        -APIKeys $Decon_Master_Configuration.DetectionModules.VirusTotalDetectionModule.APIKey
        }
        #>

        # TODO: catch when the user file is invalid and NOT wipe out all the folder everywhere...
        $Message = "<$CurrentScriptName>: Running Update-DeconFileStructure"
        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message "<$CurrentScriptName>: Running Update-DeconFileStructure"
        write-verbose $Message

        Update-DeconFileStructure -Users $Decon_User_Configuration.Users.keys `
                                  -TrustHomeDirectory $Decon_Master_Configuration.Settings.TrustHomeFolder `
                                  -UntrustHomeDirectory $Decon_Master_Configuration.Settings.UntrustHomeFolder `
                                  -DecontaminationStagingArea $Decon_Master_Configuration.Settings.DecontaminationStagingArea `
                                  -DLPStagingArea $Decon_Master_Configuration.Settings.DLPStagingArea `
                                  -QuarantineFolder $Decon_Master_Configuration.Settings.QuarantineFolder
        <#
        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message "<$CurrentScriptName>: Running Sync-DeconFolderPermissions"
        Foreach ($User in $Decon_User_Configuration.Users.keys)
        {
            Sync-DeconFolderPermissions -UserName $Decon_User_Configuration.Users[$User].UserName `
                                        -UserTrustAccounts $Decon_User_Configuration.Users[$User].UserTrustAccounts `
                                        -UserUntrustAccounts $Decon_User_Configuration.Users[$User].UserUntrustAccounts `
                                        -UntrustHomeFolder $Decon_Master_Configuration.settings.UntrustHomeFolder `
                                        -TrustHomeFolder $Decon_Master_Configuration.settings.TrustHomeFolder `
                                        -ExpectedUserPermissions @("Read","Write","Delete","Synchronize")
        
            $Message = "<$CurrentScriptName>: Sync-DeconFolderPermissions " + "-UserName " + $Decon_User_Configuration.Users[$User].UserName + " -UserTrustAccounts " + $Decon_User_Configuration.Users[$User].UserTrustAccounts + " -UserUntrustAccounts " + $Decon_User_Configuration.Users[$User].UserUntrustAccounts + " -UntrustHomeFolder " + $Decon_Master_Configuration.settings.UntrustHomeFolder + " -TrustHomeFolder " + $Decon_Master_Configuration.settings.TrustHomeFolder +" -ExpectedUserPermissions " + @("Read","Write","Delete","Synchronize")
            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message $Message
        }
        #>  

        $Message = "<$CurrentScriptName>: executing Sync-WMIFolderMonitors"
        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
        write-verbose $Message
    
        Sync-WMIFolderMonitors -Interval 2 `
                               -DecontaminationStagingArea $Decon_Master_Configuration.Settings.DecontaminationStagingArea `
                               -DLPStagingArea $Decon_Master_Configuration.Settings.DLPStagingArea `
                               -Users $Decon_User_Configuration.Users.keys `

        # Cache the FileDB to disk
        Export-Clixml -Path "$PSScriptRoot\Config\DeconDB.xml" -InputObject $FileDB -Force

        $HealthChecksNeedRun = $False
        $counter = 1
        while(-not($HealthChecksNeedRun))
        {
            Sync-DeconFiles -TrustHomeDirectory $Decon_Master_Configuration.Settings.TrustHomeFolder `
                            -UntrustHomeDirectory $Decon_Master_Configuration.Settings.UntrustHomeFolder `
                            -DecontaminationStagingArea $Decon_Master_Configuration.Settings.DecontaminationStagingArea `
                            -DLPStagingArea $Decon_Master_Configuration.Settings.DLPStagingArea `
                            -FileDB ([ref]$FileDB) `
                            -ActionPreference $Decon_Master_Configuration.Settings.ActionPreference
                            
            $Message = "<$CurrentScriptName>: Sync-DeconFiles -TrustHomeDirectory " + $Decon_Master_Configuration.Settings.TrustHomeFolder + 
                                                            " -UntrustHomeDirectory " + $Decon_Master_Configuration.Settings.UntrustHomeFolder + 
                                                            " -DecontaminationStagingArea " + $Decon_Master_Configuration.Settings.DecontaminationStagingArea + 
                                                            " -DLPStagingArea " + $Decon_Master_Configuration.Settings.DLPStagingArea + 
                                                            " -FileDB `$FileDB"

            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message $Message
            #Write-Verbose -message "<$CurrentScriptName>: Executing Sync-DeconFiles"

            $Decon_Events = Get-Event -SourceIdentifier *_WMI_Folder_Monitor
        
            foreach($Event in $Decon_Events)
            {
                $Splat = @{
                    'DecontaminationStagingArea' = $Decon_Master_Configuration.Settings.DecontaminationStagingArea;
                    'DLPStagingArea' = $Decon_Master_Configuration.Settings.DLPStagingArea;
                    'EventID' = $Event.EventIdentifier;
                    'FilePath' = $Event.SourceArgs.NewEvent.TargetInstance.Name;
                    'UserName' = ($Event.SourceArgs.NewEvent.TargetInstance.name).split("\")[-3];
                    'StagingAreaName' = ($Event.SourceArgs.NewEvent.TargetInstance.name).split("\")[-4];
                    'UnpackingToolLocation' = $Decon_Master_Configuration.Settings.UnpackingToolLocation;
                    'ActionPreference' = $Decon_Master_Configuration.Settings.ActionPreference;
                    'TrustHomeFolder' = $Decon_Master_Configuration.Settings.TrustHomeFolder;
                    'UntrustHomeFolder' = $Decon_Master_Configuration.Settings.UntrustHomeFolder;
                    'DetectionModulesSettings' = $Decon_Master_Configuration.DetectionModules;
                    'ActionThreshHold' = $Decon_Master_Configuration.Settings.ActionThreshHold;
                    'FileDB' = $FileDB
                    'QuarantineFolder' = $Decon_Master_Configuration.Settings.QuarantineFolder
                }
                
                $ModuleName = 'Decon'

                $ScriptBlock = {
               
                Import-Module -Name $args[0]
                Invoke-DetectionModuleManager -DecontaminationStagingArea $args[1].DecontaminationStagingArea `
                                              -DLPStagingArea $args[1].DLPStagingArea`
                                              -EventID $args[1].EventID `
                                              -FilePath $args[1].FilePath `
                                              -UserName $args[1].UserName `
                                              -StagingAreaName $args[1].StagingAreaName `
                                              -UnpackingToolLocation $args[1].UnpackingToolLocation `
                                              -ActionThreshHold $args[1].ActionThreshHold `
                                              -TrustHomeFolder $args[1].TrustHomeFolder `
                                              -UnTrustHomeFolder $args[1].UnTrustHomeFolder `
                                              -DetectionModulesSettings $args[1].DetectionModulesSettings `
                                              -ActionPreference $args[1].ActionPreference `
                                              -FileDB $args[1].FileDB `
                                              -QuarantineFolder $args[1].QuarantineFolder 
                }

                if ($parallel)
                {
                    $Session = New-Session -RunspaceName ("DetectionModuleManager" + $runspace.Id) -Scriptblock $ScriptBlock -Arguments $ModuleName, $Splat
                    
                    $Handle = $Session.BeginInvoke()
                    Write-Verbose "<$CurrentScriptName>: Executing in multi thread mode"
                }
                else
                {
                    Invoke-DetectionModuleManager @Splat
                    Write-Verbose "<$CurrentScriptName>: Executing in single thread mode"
                }

                $Message = "<$CurrentScriptName>: Invoke-DetectionModuleManager"
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message $Message
                Write-Verbose $Message
                
                Remove-Event -EventIdentifier $Event.EventIdentifier
            }

            
            start-sleep -seconds 1
        
            if ($counter % 20 -eq 0)
            {
                $HealthChecksNeedRun = $true
                $counter = 0
                
            }
            $counter += 1
        }
    }
}
