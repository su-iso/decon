$Scripts_Location = Join-Path $PSScriptRoot "Scripts"

$Scripts = Get-ChildItem -Path $Scripts_Location

$Configuration_Location = join-path $PSScriptRoot "Config"

# import all files in the scripts directory
foreach ($Script in $Scripts){
    . $Script.FullName
}

#Update-DeconConfig -Path "$Configuration_Location\Decon_Configuration.json"

# import detection modules
$config = get-content -path "$Configuration_Location\Decon_Configuration.json" -raw | convertfrom-json | convertfrom-JsonCustomObject
foreach ($Detection_Module in $config.DetectionModules.keys)
{
    Import-Module -name $Detection_Module
}