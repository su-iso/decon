﻿function Get-WMIEventSummary()
{
    $SummaryTable = @{"EventFilter" = @(Get-WmiObject -Namespace "root/subscription" -Class __EventFilter); `
                      "EventConsumer" = @(Get-WmiObject -Namespace "root/subscription" -Class __EventConsumer); `
                      "CommandLineEventConsumer" = @(Get-WmiObject -Namespace "root/subscription" -Class CommandLineEventConsumer); `
                      "ActiveScriptEventConsumer" = @(Get-WmiObject -Namespace "root/subscription" -Class ActiveScriptEventConsumer); `
                      "LogFileEventConsumer" = @(Get-WmiObject -Namespace "root/subscription" -Class LogFileEventConsumer); `
                      "NTEventLogEventConsumer" = @(Get-WmiObject -Namespace "root/subscription" -Class NTEventLogEventConsumer); `
                      "SMTPEventConsumer" = @(Get-WmiObject -Namespace "root/subscription" -Class SMTPEventConsumer);
                      "__SystemClass" = @(Get-WmiObject -Namespace "root/subscription" -Class __SystemClass);}
    
    return $SummaryTable
}