﻿function Mount-CIFSShare()
{
  Param(
    $UserInformationFile = 'C:\DeconProject\Config\CIFS_Admin_Account.txt',
    $LocalShareName = 'PAW_Untrust_Folder',
    $PSProvider = 'FileSystem',
    $RemoteShareName = '\\secure-iso\paw$',
    $Description = 'PAW CIFS Share',
    $Credential
  )

  # You must be on the VPN to make this work until the code is running on the Decon Server.
  # This takes some time. Kerberos must be slowing it down dramatically.
  if (-not(Get-PSDrive -Name $LocalShareName -ErrorAction SilentlyContinue))
  {
    $null = New-PSDrive -Name $LocalShareName -PSProvider $PSProvider -Root $RemoteShareName -Description $Description -Credential $cred
  }
}



