﻿$ScriptsLocation = Join-Path $PSScriptRoot "Scripts"

$Scripts = Get-ChildItem -Path $ScriptsLocation

# import all files in the scripts directory
foreach ($Script in $Scripts){
    . $Script.FullName
}

# import the controller script
. "$PSScriptRoot\VirusTotalRequestHandler.ps1"

# importing the POSH VT module that this module depends on
Import-Module -Name Posh-VirusTotal