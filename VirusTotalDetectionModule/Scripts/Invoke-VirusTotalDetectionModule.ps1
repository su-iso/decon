﻿function Invoke-VirusTotalDetectionModule()
{
    Param(
    [parameter(Mandatory=$true,
               ValueFromPipelineByPropertyName=$true)]
    [string] $FilePath,

    # The key needs removed from the code and added to the configuration file.
    # The app should get the key formt he configurati
    [parameter(ValueFromPipelineByPropertyName=$true)]
    [string] $VTAPIKey = 'e6f5d0524fe55290251482a429aad8ef1785a6ef9bd3ffe99e5180d205865198'
    )

    $VerbosePreference = 'continue'

    <#
    This function need to have logic around the file size. 
    If the file size is greater than the max size alloud by VT then
    it need to return something back to DMM saying it could 
    not be scanned.
    #>

    $CurrentScriptName = $MyInvocation.MyCommand.Name

    $Hash = (Get-FileHash -Path $FilePath -Algorithm SHA256).Hash
    
    $FileReportObtained = $false

    while (-not($FileReportObtained))
    {
        write-verbose $Hash
        write-verbose $FilePath
        write-verbose $VTAPIKey
        $Request_Return = Get-VTFileReport -Resource $Hash -APIKey $VTAPIKey

        $Message = "<$CurrentScriptName>: Get-VTFileReport -Resource $Hash -APIKey $VTAPIKey"  
        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message $Message

        switch ($Request_Return)
        {
            {$_.Response_Code -eq 0}
            {
                Submit-VTFile -File $FilePath -APIKey $VTAPIKey
        
                $Message = "<$CurrentScriptName>: File Not Found. Submitting File to be scanned by VT: $FilePath" 
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message

                Start-Sleep -Seconds 15
            }

            {$_.Response_Code -eq 1} 
            {
                $FileReportObtained = $true

                $Message = "<$CurrentScriptName>: File Found! Generating Report Obtained event" 
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message $Message
            }

            {$_.Response_Code -eq -2} 
            {
                $Message = "<$CurrentScriptName>: File still Queued for scanning in Virus Total"
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message

                Start-Sleep -Seconds 15
            }

            {$_.length -eq 0} 
            {
                $Message = "<$CurrentScriptName>: VT API Key on cooldown: $VTAPIKey"
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message

                Start-Sleep -Seconds 15
            }
        }
    }
        
    $Message = "<$CurrentScriptName>: Returning Report to Detection Module Manager."
    Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message $Message
    
    # The ModuleName should not be hard coded in to this hashtable
    # preferable it would be able to get its name from the command name 
    # or it would be passed into the function
    
    $Report = @{"Hits" = $Request_Return.positives; 
                "Hash" = $Request_Return.sha256; 
                "ReportDate" = [datetime]$Request_Return.scan_date
                "ModuleName" = "VirusTotalDetectionModule"
                }

    return $Report
}