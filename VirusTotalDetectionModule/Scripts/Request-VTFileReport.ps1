﻿function Request-VTFileReport() 
{
    Param(
    [parameter(Mandatory=$true)]
    [string]$Hash,
    [parameter(Mandatory=$true)]
    [string]$FilePath
    )

    $EventID = Get-Random -Minimum 0 -Maximum 1000000000
    $VTEventType = "VT_File_Report_Requested"
    $EventArguments = @{FilePath=$FilePath; Hash=$Hash; VTEventType=$VTEventType; EventID=$EventID}
    New-Event -SourceIdentifier "$VTEventType`_$EventID" -Sender Request-VTFileReport -EventArguments $EventArguments

    $Report = Wait-Event -SourceIdentifier "VT_File_Report_Obtained_$RequestID"

    $Report | Out-Host
}