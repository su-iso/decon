﻿function Watch-VTQue()
{
    Param(
    [parameter(Mandatory=$true)]
    [string]$VTAPIKeyFile
    )

    $Time_Counter = 0
    while($true)
    {
        if ($VTAPIKeyFile)
        {
            $VTAPIKeyList = (Get-content -Path $VTAPIKeyFile).split(",")
        }

        if ($Time_Counter % 4 -eq 0)
        {
            # TODO: this would look for the queued file to be scanned
            $Events = Get-Event -SourceIdentifier VTFileScanQueued* -ErrorAction SilentlyContinue
            $Events += Get-Event -SourceIdentifier VTFileReportRequested* -ErrorAction SilentlyContinue
        }
        else
        {
            $Events = Get-Event -SourceIdentifier VTFileReportRequested* -ErrorAction SilentlyContinue
            $Events += Get-Event -SourceIdentifier VTFileScanQueued* -ErrorAction SilentlyContinue
        }

        $Key_Counter = 0
        foreach ($Event in $Events[0 .. ($VTAPIKeyList.Count - 1)])
        {
            if ($Event)
            {
                $Hash = $Event.SourceArgs.Hash 
                $FilePath = $Event.SourceArgs.FilePath
                $VTEventType = $Event.SourceIdentifier.Split("_")[0]
                $EventID = $Event.SourceIdentifier.Split("_")[1]
                $VTAPIKey = $VTAPIKeyList[$Key_Counter]
                Resolve-VTFileRequest -Hash $Hash -FilePath $FilePath -VTEventType $VTEventType -EventID $EventID -VTAPIKey $VTAPIKey
            }
            $Key_Counter += 1
        }
        $Time_Counter += 1
        Start-Sleep -Seconds 15
    }
}