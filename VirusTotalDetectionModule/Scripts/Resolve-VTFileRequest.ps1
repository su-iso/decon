﻿function Resolve-VTFileRequest()
{
    Param(
        [parameter(Mandatory=$true)]
        $Hash,

        [parameter(Mandatory=$true)]
        $FilePath,

        [parameter(Mandatory=$true)]
        $VTEventType,

        [parameter(Mandatory=$true)]
        $VTAPIKey,
        
        [parameter(Mandatory=$true)]
        $RunspaceID
        )

    $CurrentScriptName = $MyInvocation.MyCommand.Name

    $Request_Return = Get-VTFileReport -Resource $Hash -APIKey $VTAPIKey

    $Message = "<$CurrentScriptName>: Get-VTFileReport -Resource $Hash -APIKey $VTAPIKey"  
    Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message $Message

    switch ($Request_Return)
    {
        {$_.Response_Code -eq 0}
        {
            # This if statement shouldnt need to exist but it appears that VT is not updating is API's database as fast as we are rechecking for the file to be finished
            # By checking the kind of event that is being resolved we can see if we need to requeue the file to be scanned.
            if (-not($VTEventType -eq "VT_File_Scan_Queued"))
            {
                Submit-VTFile -File $FilePath -APIKey $VTAPIKey
                $EventArguments = @{FilePath=$FilePath; Hash=$Hash; VTEventType="VT_File_Scan_Queued"; RunspaceID=$RunspaceID}
                Remove-Event -SourceIdentifier "$VTEventType`_$RunspaceID"
                New-Event -SourceIdentifier "VT_File_Scan_Queued_$RunspaceID" -Sender Resolve-VTFileRequest -EventArguments $EventArguments

                $Message = "<$CurrentScriptName>: File Not Found. Submitting File to be scanned by VT: $FilePath" 
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
            }
        }

        {$_.Response_Code -eq 1} 
        {
            # "File Found. Examining Report." | out-host
            Remove-Event -SourceIdentifier "$VTEventType`_$RunspaceID"
            $EventArguments = @{FilePath=$FilePath; Hash=$Hash; VTEventType="VT_File_Report_Obtained"; RunspaceID=$RunspaceID; Report=$Request_Return}
            #New-Event -SourceIdentifier "VT_File_Report_Obtained_$RunspaceID" -Sender Resolve-VTFileRequest -EventArguments $EventArguments
            (get-runspace -Id $RunspaceID).Events.GenerateEvent("VT_File_Report_Obtained_$RunspaceID", "Resolve-VTFileRequest", $EventArguments, $null)

            $Message = "<$CurrentScriptName>: File Found! Generating Report Obtained event" 
            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message $Message
        }

        {$_.Response_Code -eq -2} 
        {
            $Message = "<$CurrentScriptName>: File still Queued for scanning in Virus Total"
            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
        }

        {$_.length -eq 0} 
        {
            $Message = "<$CurrentScriptName>: VT API Key on cooldown: $VTAPIKey"
            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
        }
    }
}