﻿function VirusTotalRequestHandler()
{
    Param(
    [parameter(Mandatory=$true)]
    [string[]]$VTAPIKeyList
    )

    $CurrentScriptName = $MyInvocation.MyCommand.Name
    $Message = (Get-Date) + " <$CurrentScriptName>: Starting Script"  
    Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message

    if(-not(get-module -name Posh-VirusTotal)) 
    {
        try
        {
            Import-Module -name Posh-VirusTotal
        }
        catch
        {
            $Message = (Get-Date) + "<$CurrentScriptName>: Failed to import Posh-VirusTotal Module."  
            Write-EventLog -LogName Decon -source Decon -EntryType Error -EventId 2000 -Message $Message 
        }
    }

    $Time_Counter = 0
    while($true)
    {
        $Message = "<$CurrentScriptName>: Checking for VT Events"   
        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message 
        
        if ($Time_Counter % 5 -eq 0)
        {
            # Gets the events for files pushed into VT to be scanned first.
            $Events = Get-Event -SourceIdentifier VT_File_Scan_Queued* -ErrorAction SilentlyContinue
            
            # adds the rest of the events that need VT checked for reports
            $Events += Get-Event -SourceIdentifier VT_File_Report_Requested* -ErrorAction SilentlyContinue
        }
        else
        {
            # Reverse of above
            $Events = Get-Event -SourceIdentifier VT_File_Report_Requested* -ErrorAction SilentlyContinue
            $Events += Get-Event -SourceIdentifier VT_File_Scan_Queued* -ErrorAction SilentlyContinue
        }

        $Message = "<$CurrentScriptName>: Found " + $Events.count + " VT Events"   
        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message   
        
        if ($Events.count -gt 1)
        {
            $Message = "<$CurrentScriptName>: Starting to process VT Events"  
            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message  

            $Key_Counter = 0
            foreach ($Event in $Events[0 .. ($VTAPIKeyList.Count - 1)])
            {
                $Message = "<$CurrentScriptName>: Processing Event for file: " + $Event.SourceArgs.FilePath 
                Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message  
                
                if (-not($Event -eq $null))
                {
                    $Hash = $Event.SourceArgs.Hash 
                    $FilePath = $Event.SourceArgs.FilePath
                    $VTEventType = $Event.SourceArgs.VTEventType
                    $RunspaceID = $Event.SourceArgs.RunspaceID
                    $VTAPIKey = $VTAPIKeyList[$Key_Counter]

                    $Message = "<$CurrentScriptName>: Resolve-VTFileRequest -Hash $Hash -FilePath $FilePath -VTEventType $VTEventType -VTAPIKey $VTAPIKey -RunspaceID $RunspaceID"
                    Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message $Message  
                    
                    Resolve-VTFileRequest -Hash $Hash -FilePath $FilePath -VTEventType $VTEventType -VTAPIKey $VTAPIKey -RunspaceID $RunspaceID
                        
                }
            $Key_Counter += 1
            }
        }
        elseif($Events.count -eq 1)
        {
            $Message = "<$CurrentScriptName>: Processing Event for file: " + $Events.SourceArgs.FilePath
            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message
                
            $Hash = $Events.SourceArgs.Hash
            $FilePath = $Events.SourceArgs.FilePath
            $VTEventType = $Events.SourceArgs.VTEventType
            $RunspaceID = $Events.SourceArgs.RunspaceID
            $VTAPIKey = $VTAPIKeyList[0]

            $Message = "<$CurrentScriptName>: Resolve-VTFileRequest -Hash $Hash -FilePath $FilePath -VTEventType $VTEventType -VTAPIKey $VTAPIKey -RunspaceID $RunspaceID"
            Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 1000 -Message $Message  

            Resolve-VTFileRequest -Hash $Hash -FilePath $FilePath -VTEventType $VTEventType -VTAPIKey $VTAPIKey -RunspaceID $RunspaceID
        }
        
        $Time_Counter += 1
        $Message = "<$CurrentScriptName>: Sleeping 15 seconds before Checking for VT events."
        Write-EventLog -LogName Decon -source Decon -EntryType Information -EventId 3000 -Message $Message  
 
        Start-Sleep -Seconds 15
    }
}